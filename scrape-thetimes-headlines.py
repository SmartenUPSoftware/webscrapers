# Get the daily headlines from SliceCollection on thetimes.co.uk
'''
    
    pip3 install BeautifulSoup
    pip3 install requests
'''

# Import sys
import sys

# Import libraries
import requests
import urllib.request
import time
from bs4 import BeautifulSoup

# We want to use python 3 for this
if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

# Webscrape from
url = 'https://www.thetimes.co.uk/'

# Fetch the url
response = requests.get(url)

# Parse HTML
soup = BeautifulSoup(response.text, "html.parser")

# Get SliceCollection content
sliceCollection = soup.find('div', 'SliceCollection')

# To download the whole data set, let's do a for loop through all a tags
for headlineHtml in sliceCollection.findAll('h3', 'Item-headline'): #'a' tags are for links
    # There's a "a" tag nested in the headline
	headlineTag = headlineHtml.find('a')
    # Get the text and Print
    # Right now we print it immediatly we could append it to a csv file
	print(headlineTag.contents[0])
