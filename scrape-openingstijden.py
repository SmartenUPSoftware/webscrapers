# Get the daily headlines from SliceCollection on thetimes.co.uk

# Import sys
import sys

# Import libraries
import requests
import urllib.request
import time
import csv
from bs4 import BeautifulSoup

# We want to use python 3 for this
if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

# Set the start URL
url = 'https://www.openingstijden.nl/steden/'

# Flat output list, will hold the poi object
poi_list = []

# # Slice collection div
# SliceCollection = soup.find('div', 'SliceCollection')

# # To download the whole data set, let's do a for loop through all a tags
# for i in SliceCollection.findAll('h3', 'Item-headline'): #'a' tags are for links
# 	one_a_tag = i.find('a')
# 	print(one_a_tag.contents[0])

def parseCategory(categoryObj):
	catPage = requests.get(categoryObj['href'])
	catPageHTML = BeautifulSoup(catPage.text, "html.parser")
	results = catPageHTML.find(id="results-ajax")

	if not results:
		return

	# get all poi's
	pois = results.findAll('li')

	for poi in pois:
		text = poi.find('h3')
		latlng = poi.get('data-geo')
		# Check if the text exists
		if (text):
			text = poi.find('h3')
			name = text.find('a').contents[2].strip()
			address = text.find('a').find('span').contents[1]
			city = text.find('a').find('cite').contents[0]
			poi_list.append([
				name,
				address,
				city,
				latlng
			])

# Parse city pages
def parseCity(cityObj):
	# Get the page with all the categories for that city
	cityPage = requests.get(cityObj['href'])
	cityPageHTML = BeautifulSoup(cityPage.text, "html.parser")
	# Get the categories container
	categoriesHTML = cityPageHTML.find('').find('div', 'opensunday-bar')
	categories = categoriesHTML.findAll('li')

	# Test just one
	# parseCategory(categories[1].find('a'))

	# Live version
	for category in categories:
		categoryObj = category.find('a')
		# Check object
		if (categoryObj):
			print('Scraping category: ' + categoryObj.contents[0].strip())
			parseCategory(categoryObj)

# Init function
def init(rootUrl):
	# Connect to the URL
	citiesPage = requests.get(rootUrl)
	# Parse html
	citiesPageHTML = BeautifulSoup(citiesPage.text, "html.parser")
	# The container
	cityContainer = citiesPageHTML.find('div', 'shoplocation-bar')
	cities = cityContainer.findAll('li')

	# Test just one
	# parseCity(cities[1].find('a'))

	# Live version
	i = 0
	for city in cities:
		# if i == 2:
		# 	break
		cityObj = city.find('a')

		if (cityObj):
			print('Scraping city: ' + cityObj.contents[0].strip())
			parseCity(cityObj)
			i += 1


init(url)

with open('results.csv', 'w') as csvFile:
    writer = csv.writer(csvFile)
    writer.writerows(poi_list)
csvFile.close()