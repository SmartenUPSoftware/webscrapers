# Get the daily headlines from SliceCollection on thetimes.co.uk
'''
		
		pip3 install BeautifulSoup
		pip3 install requests
'''

# Import sys
import sys
import os 

# Import libraries
import requests
import wget
import urllib.request
import time
from bs4 import BeautifulSoup

# We want to use python 3 for this
if sys.version_info[0] < 3:
		raise Exception("Python 3 or a more recent version is required.")

# Current file directory
dir_path = os.path.dirname(os.path.realpath(__file__))

# Webscrape from
url = 'http://www.ico.org/new_historical.asp'

# Fetch the url
response = requests.get(url)

# Parse HTML
soup = BeautifulSoup(response.text, "html.parser")

# Get SliceCollection content
allLinks = soup.findAll('a', href=True)

# To download the whole data set, let's do a for loop through all a tags
for link in allLinks: #'a' tags are for links
	# There's a "a" tag nested in the headline
	if link.text == 'Excel':
		# Construct the download url
		dowload = url + '/' + link['href']
		# Sanitize the filename
		filename = link['href'].replace('../', '').replace('/', '-')
		# Do the request
		req = requests.get(dowload)
		# Write the file
		open(dir_path + '/tmp/' + filename, 'wb').write(req.content)
		print('Download done for: ' + filename)